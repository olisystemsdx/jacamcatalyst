from asyncio.windows_events import NULL
import requests
import json
import time
import pandas as pd
import pandas.io.sql as sqlio
import psycopg2
from IPython.display import display, HTML
import logging 
import numpy
from sqlalchemy import null 
import math 
import OLIPy as API 

# enable logging
logging.basicConfig(
    format='%(asctime)s %(levelname)s'','' %(message)s',
    level=logging.INFO,
    filename='logs/file.log') #DEBUG, INFO, WARN, ERROR

# get range of values between any two values
def get_sensitivity_range(start,end,increment):
    if increment==0:
        increment = 1
    step = int(abs((start-end)/increment)+1)
    return numpy.linspace(start,end, num=step, retstep=False)

# convert the json object to a dataframe.
def get_normalized_waterAnalysis(jsonObject):
    itt = 0
    for key in jsonObject:
        df_rec = pd.json_normalize(jsonObject[key])
        waterAnalysisOutputRecord = df_rec if itt == 0 else waterAnalysisOutputRecord.append(df_rec)
        itt = itt+1
    return waterAnalysisOutputRecord

# replace string sample values with zeros
def input_data_cleanup(row):
    for  (key, val) in row.iteritems():
        if key == "wid" or key == "analysis_no":
            continue 
        elif type(val) == str:  #If below detection limit then return 0. (Labs usually store string abbrevations when measured species are below detection limit.)
            row[key] = 0.0
        elif pd.isna(val): 
            row[key] = 0.0  #If not a number then return 0
        else:
            continue 
    return row 

# convert prescale data to scaling index 
def convert_prescale_to_scalingindex(dict):
    scaling_index_dict = {}
    for key in dict :
        
        if dict[key] > 0 : 
            result = math.log10(dict[key])
            #print(f'Key: {key} -> Value: {dict[key]} -> SI: {result}')
            scaling_index_dict[key] = result
        else: 
            #print(f'Key: {key} -> Value: {dict[key]}')
            scaling_index_dict[key]  = None
    
    return scaling_index_dict

# convert mg per liter to pounds per 1000 BBL
def convert_mgl_to_per1000BBL(dict):
    for key in dict :
        dict[key] = (dict[key] * 158970) / 453000 
    
    return dict

# get result for a given JSON object path
def get_result_by_path(data, keys):
    try:
        return get_result_by_path(data[keys[0]], keys[1:]) \
            if keys else data
    except KeyError:
        return dict()
    except TypeError:
        return dict()

# validate object unit, if unit doesn't match, return null
def validate_unit_and_fetch_result(data,value_key,unit = ""):
    #print(data)
    try:
        if value_key == "":
            if 'unit' in data:
                if data['unit'] == unit:
                    #return True 
                    if 'values' in data:
                        return data['values']
                    elif 'value' in data:
                        return data['value']
                    else:
                        return None
                else:
                    return None
            else: 
                return None

        if value_key in data:     
            if 'unit' in data[value_key]:
                variable_unit = data[value_key]['unit']
                print(f'Required unit {unit}. Actual unit {variable_unit}')
                if variable_unit == unit:
                    #return True 
                    return data[value_key]['value']
                else:
                    return None
            else: 
                return None
        else:
            return None 
    except KeyError:
        print("Key not found exception")
        return None 

# given a row, create the water analysis calculation input             
def generate_wateranalysis_json(row):
    #print(f"Test - Water Analysis # : {row.analysisno}, Constituent: {row.constituent}")
    water_analysis_input = {
                            "params": {
                                "waterAnalysisInputs": [
                                    {
                                        "group": "Neutrals",
                                        "name": "CO2",
                                        "unit": "mg/L",
                                        "value": row.co2,  
                                        "charge": 0.0
                                    },
                                    {
                                        "group": "Neutrals",
                                        "name": "H2S",
                                        "unit": "mg/L",
                                        "value": row.h2s,  
                                        "charge": 0.0
                                    },
                                    {
                                        "group": "Neutrals",
                                        "name": "SIO2",
                                        "unit": "mg/L",
                                        "value": row.sio2,  
                                        "charge": 0.0
                                    },
                                    {
                                        "group": "Neutrals",
                                        "name": "BOH3",
                                        "unit": "mg/L",
                                        "value": row.borate,  
                                        "charge": 0.0
                                    },
                                    {
                                        "group": "Neutrals",
                                        "name": "NACOOH",
                                        "unit": "mg/L",
                                        "value": row.nacooh,  
                                        "charge": 0.0
                                    },
                                    {
                                        "group": "Cations",
                                        "name": "NAION",
                                        "unit": "mg/L",
                                        "value": row.na,  
                                        "charge": 1
                                    },
                                    {
                                        "group": "Cations",
                                        "name": "KION",
                                        "unit": "mg/L",
                                        "value": row.k,  
                                        "charge": 1
                                    },
                                    {
                                        "group": "Cations",
                                        "name": "CAION",
                                        "unit": "mg/L",
                                        "value": row.ca,  
                                        "charge": 2
                                    },
                                    {
                                        "group": "Cations",
                                        "name": "MGION",
                                        "unit": "mg/L",
                                        "value": row.mg,  
                                        "charge": 2 
                                    },
                                    {
                                        "group": "Cations",
                                        "name": "SRION",
                                        "unit": "mg/L",
                                        "value": row.sr,  
                                        "charge": 2
                                    },
                                    {
                                        "group": "Cations",
                                        "name": "BAION",
                                        "unit": "mg/L",
                                        "value": row.ba,  
                                        "charge": 2
                                    },
                                    {
                                        "group": "Cations",
                                        "name": "FEIIION",
                                        "unit": "mg/L",
                                        "value": row.fe, 
                                        "charge": 2
                                    },
                                    {
                                        "group": "Cations",
                                        "name": "MNION",
                                        "unit": "mg/L",
                                        "value": row.mn,  
                                        "charge": 2
                                    }
                                    ,
                                    {
                                        "group": "Cations",
                                        "name": "LIION",
                                        "unit": "mg/L",
                                        "value": row.li,  
                                        "charge": 1
                                    },
                                    {
                                        "group": "Cations",
                                        "name": "NH4ION",
                                        "unit": "mg/L",
                                        "value": row.nh4,  
                                        "charge": 1
                                    },
                                    {
                                        "group": "Cations",
                                        "name": "ALION",
                                        "unit": "mg/L",
                                        "value": row.al,  
                                        "charge": 3
                                    },
                                    {
                                        "group": "Cations",
                                        "name": "ZNION",
                                        "unit": "mg/L",
                                        "value": row.zn,  
                                        "charge": 2
                                    },
                                    {
                                        "group": "Cations",
                                        "name": "PBION",
                                        "unit": "mg/L",
                                        "value": row.pb,  
                                        "charge": 2
                                    },
                                    {
                                        "group": "Cations",
                                        "name": "CRIIIION",
                                        "unit": "mg/L",
                                        "value": row.criii,  
                                        "charge": 3
                                    },
                                    {
                                        "group": "Cations",
                                        "name": "NIION",
                                        "unit": "mg/L",
                                        "value": row.ni,  
                                        "charge": 2
                                    },
                                    {
                                        "group": "Cations",
                                        "name": "CUION",
                                        "unit": "mg/L",
                                        "value": row.cu,  
                                        "charge": 2
                                    },
                                    {
                                        "group": "Anions",
                                        "name": "CLION",
                                        "unit": "mg/L",
                                        "value": row.cl,  
                                        "charge": -1
                                    },
                                    {
                                        "group": "Anions",
                                        "name": "SO4ION",
                                        "unit": "mg/L",
                                        "value": row.so4,  
                                        "charge": -2
                                    },
                                    {
                                        "group": "Anions",
                                        "name": "HCO3ION",
                                        "unit": "mg/L",
                                        "value": row.hco3,  
                                        "charge": -1
                                    },
                                    {
                                        "group": "Anions",
                                        "name": "FION",
                                        "unit": "mg/L",
                                        "value": row.f,  
                                        "charge": -1
                                    },
                                    {
                                        "group": "Anions",
                                        "name": "BRION",
                                        "unit": "mg/L",
                                        "value": row.br,  
                                        "charge": -1
                                    },
                                    {
                                        "group": "Anions",
                                        "name": "IODION",
                                        "unit": "mg/L",
                                        "value": row.iod,  
                                        "charge": -1
                                    },
                                    {
                                        "group": "Anions",
                                        "name": "PO4ION",
                                        "unit": "mg/L",
                                        "value":  row.po4,  
                                        "charge": -3
                                    },
                                    {
                                        "group": "Anions",
                                        "name": "CO3ION",
                                        "unit": "mg/L",
                                        "value":  row.co3,  
                                        "charge": -2
                                    },
                                    {
                                        "group": "Anions",
                                        "name": "NO3ION",
                                        "unit": "mg/L",
                                        "value":  row.no3,  
                                        "charge": -1
                                    },
                                    {
                                        "group": "Anions",
                                        "name": "ACETATEION",
                                        "unit": "mg/L",
                                        "value":  row.acetate,  
                                        "charge": -1
                                    },
                                    {
                                        "group": "Anions",
                                        "name": "PROPANATEION",
                                        "unit": "mg/L",
                                        "value":  row.propanate,  
                                        "charge": -1
                                    },
                                    {
                                        "group": "Anions",
                                        "name": "BUTANION",
                                        "unit": "mg/L",
                                        "value":  row.butan,  
                                        "charge": -1
                                    },
                                    {
                                        "group": "Properties",
                                        "name": "Temperature",
                                        "unit": "°F",
                                        "value": row.sample_temp,  
                                    },
                                    {
                                        "group": "Properties",
                                        "name": "Pressure",
                                        "unit": "psig", 
                                        "value": row.sample_pressure
                                    },
                                    {
                                        "group": "Electroneutrality Options",
                                        "name": "ElectroNeutralityBalanceType",
                                        "value": "MakeupIon" 
                                    },
                                    {
                                        "group": "Electroneutrality Options",
                                        "name": "MakeupIonBaseTag",
                                        "value": "CLION" 
                                    },
                                    {
                                        "group": "Calculation Options",
                                        "name": "AllowSolidsToForm",
                                        "value": False  
                                    },
                                    {
                                        "group": "Calculation Options",
                                        "name": "CalcType",
                                        "value": "EquilCalcOnly" #"value": "ReconcilePh"
                                    },
                                    {
                                        "group": "Calculation Options",
                                        "name": "CalcAlkalnity",
                                        "value": True  
                                    }
                                    ,
                                    {
                                        "group": "Calculation Options",
                                        "name": "AlkalinityPhTitrant",
                                        "value": "H2SO4"
                                    }
                                    ],
                                    "optionalProperties": {
                                        "electricalConductivity": True,
                                        "viscosity": True,
                                        "thermalConductivity": True,
                                        "prescalingTendencies": True,
                                        "prescalingTendenciesRigorous": True,
                                        "totalDissolvedSolids" : True, 
                                        "hardness" : True ,
                                        "materialBalanceGroup" : True
                                    },
                                    "unitSetInfo": {
                                        "liq1_phs_comp": "mg", 
                                        "solid_phs_comp": "mg",  
                                        "vapor_phs_comp": "mg", 
                                        "liq2_phs_comp": "mg",  
                                        "combined_phs_comp": "mg", 
                                        "pt": "psig",
                                        "t": "°F",
                                        "enthalpy": "J",
                                        "density": "g/m3",
                                        "mass": "mg",   
                                        "vol": "L"
                                    },
                                    "molecularConversion": {
                                    "option": "inflowRateBased"
                                    }
                                }
                            }

    return water_analysis_input 

# with temperature, pressure and inflow data; create the TP flash calculation input
def generate_iso_json(pres, temp, inflows_for_tp_survey):
    #print(f"Printing from iso function - Analysis # : {row.analysisno}, Constituent: {row.constituent}")
    iso_flash_input  = {
                            "params": {
                                "temperature": {
                                    "value": temp, 
                                    "unit": "°F"
                                },
                                "pressure": {
                                    "value": pres , 
                                    "unit": "psig"
                                },
                                "inflows": inflows_for_tp_survey
                                ,
                                # When total flow amount is required, it should be inserted inside the inflows. Currently,totalAmount only supports mole and molecular fractions. 
                                # Please Reach out to OLI if this section needs to be included
                                # "totalAmount": {
                                #     "value": 1000,
                                #     "unit": "mol"
                                # },
                                "optionalProperties": {
                                        "electricalConductivity": True,
                                        "viscosity": True,
                                        "thermalConductivity": True,
                                        "prescalingTendencies": True,
                                        "prescalingTendenciesRigorous": True,
                                        "totalDissolvedSolids" : True, 
                                        "hardness" : True ,
                                        "materialBalanceGroup" : True
                                    },
                                "unitSetInfo": {
                                    "liq1_phs_comp": "mg/L",
                                    "solid_phs_comp": "mg/L",
                                    "vapor_phs_comp": "mg/L",
                                    "liq2_phs_comp": "mg/L",
                                    "combined_phs_comp": "mg/L",
                                    "pt": "psig",
                                    "t": "°F",
                                    "enthalpy": "J",
                                    "density": "g/m3",
                                    "mass": "mg",
                                    "vol": "L"
                                },
                                "molecularConversion": {
                                    "option": "inflowRateBased"
                                }
                            }
                        }

    return iso_flash_input

# Catalyst Water Analysis and TP Flash caculations
if __name__ == "__main__":
    oliapi = API.OLIApi("username", "password")
    if oliapi.login():

        '''
        UPLOAD CHEMISTRTY MODEL FILE(S)
        Upload chemistry model files for your calculations. This can be obtained from OLI Studio
        under the File Viewer tab for each calculation object. The files need to be uploaded only 
        once. Subsequently file ids can be used
        '''
        
        # upload brine chemistry model
        #result = oliapi.upload_dbs_file("catalyst_water_analysis_01.20.2022.dbs")
        #print(json.dumps(result, indent=2))

        #chemistry_file_id = result["file"][0]["id"]
        #print(chemistry_file_id)
        #water_analysis_chemistry_file_id = result["file"][0]["id"]

        # display all available dbs files
        # result = oliapi.get_user_dbs_files()
        # print(json.dumps(result, indent=2))

        # hardcode chemistry file id or retreive this data from your database
        water_analysis_chemistry_file_id = 'ac076924-243a-4928-9d43-c8c6eb998f0f' 

        # get chemistry information
        #result = oliapi.call("chemistry-info", water_analysis_chemistry_file_id)
        #print(json.dumps(result, indent=2))  
        #with open("Data/Chem_file.json", 'w', encoding='utf-8') as f:
        #    json.dump(result, f, ensure_ascii=False, indent=4)

        # retreive sample data from your database
        con = psycopg2.connect(database="postgres", user="postgres", password="admin", host="127.0.0.1", port="5432")
        sql = "SELECT wid,analysis_no,sample_temp,sample_pressure,density,tds,conductivity,co2,h2s,cl,hco3,so4,po4,borate,ph_sampling,na,mg,ca,sr,ba,fe,k,mn,li,nh4,al,zn,pb,criii,ni,cu,f,br,iod,co3,no3,acetate,propanate,butan,sio2,nacooh from catalyst_data_table"
        inputRecords = sqlio.read_sql_query(sql, con)

        i = 0

        # for each sample data, perform water analysis followed by TP survey
        for index, row in inputRecords.iterrows(): 
            print(f"Working on sample # : {row.analysis_no}, sample temperature: {row.sample_temp}")
            print("*************************************************************")
            # add logic to exclude processed samples. Also, reprocessing failed samples one or two times may be ideal.
            if row.analysis_no == 20003: 
                print(f"run: {i} : {row.analysis_no}")
            else: 
                print(f"skip: {i} : {row.analysis_no}")
                continue 
            
            # sample data cleaning, string values are replaced with 0. You can also replace string like "Below Detection limit" with detection limit value.
            row_cleansed = input_data_cleanup(row)

            '''
            JSON INPUT FOR BRINE ANALYSIS CALCULATION
            Create the JSON input for the brine analysis calculation(water analysis) using a python dictionary. This can 
            either be read as a file and converted to a dictionary or explicitly written down as shown here. The input in 
            this dictionary apes the settings in the OLI Studio file for the water/brine calculation object
            '''
            flash_input = generate_wateranalysis_json(row_cleansed) 

            #region Step 1 : Water Analysis

            '''
            RUN WATER/BRINE ANALYSIS CALCULATION
            Use the JSON above to run the water/brine analysis calculation
            '''
            result_water_analysis = oliapi.call("wateranalysis", water_analysis_chemistry_file_id, flash_input)

            # write input and output json to file. Ensure a folder named "Data" is created first. 
            in_filename = "%s.json" % row[1]   
            with open("Data/Input_" + in_filename, 'w', encoding='utf-8') as f:
                json.dump(flash_input, f, ensure_ascii=False, indent=4)

            wa_out_filename = "%s.json" % row[1]   
            with open("Data/Water_Analysis_" + wa_out_filename, 'w', encoding='utf-8') as f:
                json.dump(result_water_analysis, f, ensure_ascii=False, indent=4)

            try:
                if "status" in result_water_analysis: 
                    status = result_water_analysis["status"]
                    if status == "FAILED":
                        print("Water analysis failed. Skip TP calculations and resume water analysis for next sample. ")
                        # water analysis failed. Update the status in your database and skip TP calcualtions. 
                        # skip TP calculations and resume water analysis for the next sample. 
                        continue 
                else: 
                    # result dataset doesn't include status. Update the status in your database and skip TP calcualtions. 
                    continue 
            except KeyError:
                continue     
            
            #region Water Analysis scaling index

            water_analysis_prescalingTendenciesRecord_path = "data/result/additionalProperties/prescalingTendencies/values"
            water_analysis_prescalingTendenciesRecord_values = get_result_by_path(result_water_analysis,water_analysis_prescalingTendenciesRecord_path.split('/'))

            #Water Analysis Scaling Index data (Unit: None)
            water_analysis_scaling_index_values = convert_prescale_to_scalingindex(water_analysis_prescalingTendenciesRecord_values)

            print("**** Water Analysis : Scaling Index ****")
            #Barium Sulfate Barite SL (BASO4). SI Unit: NA
            water_analysis_scaling_index_BASO4_value = get_result_by_path(water_analysis_scaling_index_values,['BASO4'])
            print(f'BASO4 value: {water_analysis_scaling_index_BASO4_value}')

            #Calcite (CACO3) . SI Unit: NA
            water_analysis_scaling_index_CACO3_value = get_result_by_path(water_analysis_scaling_index_values,['CACO3'])
            print(f'CACO3 value: {water_analysis_scaling_index_CACO3_value}')

            #Anhydrite (CASO4) . SI Unit: NA
            water_analysis_scaling_index_CASO4_value = get_result_by_path(water_analysis_scaling_index_values,['CASO4'])
            print(f'CASO4 value: {water_analysis_scaling_index_CASO4_value}')

            #Gypsum (CASO4.2H2O) . SI Unit: NA
            water_analysis_scaling_index_CASO42H2O_value = get_result_by_path(water_analysis_scaling_index_values,['CASO4.2H2O'])
            print(f'CASO4.2H2O value: {water_analysis_scaling_index_CASO42H2O_value}')

            #Celestite (SRSO4) . SI Unit: NA
            water_analysis_scaling_index_SRSO4_value = get_result_by_path(water_analysis_scaling_index_values,['SRSO4'])
            print(f'SRSO4 value: {water_analysis_scaling_index_SRSO4_value}')

            #Sphalerite (ZNS) . SI Unit: NA
            water_analysis_scaling_index_ZNS_value = get_result_by_path(water_analysis_scaling_index_values,['ZNS'])
            print(f'Sphalerite value: {water_analysis_scaling_index_ZNS_value}')

            #Witherite (BACO3) . SI Unit: NA
            water_analysis_scaling_index_BACO3_value = get_result_by_path(water_analysis_scaling_index_values,['BACO3'])
            print(f'BACO3 value: {water_analysis_scaling_index_BACO3_value}')

            #Halite (NACL) . SI Unit: NA
            water_analysis_scaling_index_NACL_value = get_result_by_path(water_analysis_scaling_index_values,['NACL'])
            print(f'NACL value: {water_analysis_scaling_index_NACL_value}')

            #Siderite (FEIICO3) . SI Unit: NA
            water_analysis_scaling_index_FEIICO3_value = get_result_by_path(water_analysis_scaling_index_values,['FEIICO3'])
            print(f'FEIICO3 value: {water_analysis_scaling_index_FEIICO3_value}')

            #Mackinawite (MACKINAWITE) . SI Unit: NA
            water_analysis_scaling_index_MACKINAWITE_value = get_result_by_path(water_analysis_scaling_index_values,['MACKINAWITE'])
            print(f'MACKINAWITE value: {water_analysis_scaling_index_MACKINAWITE_value}')

            #Galena (PBS) . SI Unit: NA
            water_analysis_scaling_index_PBS_value = get_result_by_path(water_analysis_scaling_index_values,['PBS'])
            print(f'PBS value: {water_analysis_scaling_index_PBS_value}')

            #Strontianite (SRCO3) . SI Unit: NA
            water_analysis_scaling_index_SRCO3_value = get_result_by_path(water_analysis_scaling_index_values,['SRCO3'])
            print(f'SRCO3 value: {water_analysis_scaling_index_SRCO3_value}')

            #endregion

            print("**** Water Analysis : Liquid properties ****")

            #region Water Analysis properties
            water_analysis_liquid1_properties_path = "data/result/phases/liquid1/properties"
            water_analysis_liquid1_properties = get_result_by_path(result_water_analysis,water_analysis_liquid1_properties_path.split('/'))
            #print(water_analysis_liquid1_properties)

            water_analysis_liquid1_hardness = validate_unit_and_fetch_result(water_analysis_liquid1_properties,"hardness","mg/L as CaCO3")
            print(f'hardness value: {water_analysis_liquid1_hardness}')
            water_analysis_liquid1_absoluteViscosity = validate_unit_and_fetch_result(water_analysis_liquid1_properties,"absoluteViscosity","cP")
            print(f'absoluteViscosity value: {water_analysis_liquid1_absoluteViscosity}')
            water_analysis_liquid1_pH = validate_unit_and_fetch_result(water_analysis_liquid1_properties,"ph")
            print(f'pH value: {water_analysis_liquid1_pH}')
            water_analysis_liquid1_alkalinity = validate_unit_and_fetch_result(water_analysis_liquid1_properties,"alkalinity","mg HCO3/L")
            print(f'alkalinity value: {water_analysis_liquid1_alkalinity}')
            water_analysis_liquid1_density = validate_unit_and_fetch_result(water_analysis_liquid1_properties,"density","g/m3")
            #Convert denisity from g/m3 to g/cm3. 
            water_analysis_liquid1_density = water_analysis_liquid1_density * 0.000001
            print(f'density value: {water_analysis_liquid1_density}')
            water_analysis_liquid1_specificElectricalConductivity = validate_unit_and_fetch_result(water_analysis_liquid1_properties,"specificElectricalConductivity","mho/cm")
            print(f'specificElectricalConductivity value: {water_analysis_liquid1_specificElectricalConductivity}')
            water_analysis_liquid1_totalDissolvedSolids = validate_unit_and_fetch_result(water_analysis_liquid1_properties,"totalDissolvedSolids","mg/L")
            print(f'totalDissolvedSolids value: {water_analysis_liquid1_totalDissolvedSolids}')

            #endregion 

            print("**** Water Analysis : Water concentration ****")
            
            #region Water Analysis liquid1 concentration 
            #Water Analysis liquid1 trueConcentration data (Unit: mg)
            water_analysis_liquid1_trueConcentration_path = "data/result/phases/liquid1/trueConcentration"
            water_analysis_liquid1_trueConcentration = get_result_by_path(result_water_analysis,water_analysis_liquid1_trueConcentration_path.split('/'))
            water_analysis_liquid1_trueConcentration_values = validate_unit_and_fetch_result(water_analysis_liquid1_trueConcentration,"","mg") 
            #print(water_analysis_liquid1_trueConcentration_values)

            #Aqueous CO2 (CO2). Unit: mg
            water_analysis_liquid1_trueConcentration_CO2_value = get_result_by_path(water_analysis_liquid1_trueConcentration_values,['CO2'])
            print(f'CO2 value: {water_analysis_liquid1_trueConcentration_CO2_value}')

            #Aqueous H2S (H2S). Unit: mg
            water_analysis_liquid1_trueConcentration_H2S_value = get_result_by_path(water_analysis_liquid1_trueConcentration_values,['H2S'])
            print(f'H2S value: {water_analysis_liquid1_trueConcentration_H2S_value}')

            #Barium Sulfate Barite SL (BASO4). Unit: mg
            water_analysis_liquid1_trueConcentration_BASO4_value = get_result_by_path(water_analysis_liquid1_trueConcentration_values,['BASO4'])
            print(f'BASO4 value: {water_analysis_liquid1_trueConcentration_BASO4_value}')

            #Calcite (CACO3) . Unit: mg
            water_analysis_liquid1_trueConcentration_CACO3_value = get_result_by_path(water_analysis_liquid1_trueConcentration_values,['CACO3'])
            print(f'CACO3 value: {water_analysis_liquid1_trueConcentration_CACO3_value}')

            #Anhydrite (CASO4) . Unit: mg
            water_analysis_liquid1_trueConcentration_CASO4_value = get_result_by_path(water_analysis_liquid1_trueConcentration_values,['CASO4'])
            print(f'CASO4 value: {water_analysis_liquid1_trueConcentration_CASO4_value}')

            #Gypsum (CASO4.2H2O) . Unit: mg
            water_analysis_liquid1_trueConcentration_CASO42H2O_value = get_result_by_path(water_analysis_liquid1_trueConcentration_values,['CASO4.2H2O'])
            print(f'CASO4.2H2O value: {water_analysis_liquid1_trueConcentration_CASO42H2O_value}')

            #Celestite (SRSO4) . Unit: mg
            water_analysis_liquid1_trueConcentration_SRSO4_value = get_result_by_path(water_analysis_liquid1_trueConcentration_values,['SRSO4'])
            print(f'SRSO4 value: {water_analysis_liquid1_trueConcentration_SRSO4_value}')

            #Sphalerite (ZNS) . Unit: mg
            water_analysis_liquid1_trueConcentration_ZNS_value = get_result_by_path(water_analysis_liquid1_trueConcentration_values,['ZNS'])
            print(f'Sphalerite value: {water_analysis_liquid1_trueConcentration_ZNS_value}')

            #Witherite (BACO3) . Unit: mg
            water_analysis_liquid1_trueConcentration_BACO3_value = get_result_by_path(water_analysis_liquid1_trueConcentration_values,['BACO3'])
            print(f'BACO3 value: {water_analysis_liquid1_trueConcentration_BACO3_value}')

            #Halite (NACL) . Unit: mg
            water_analysis_liquid1_trueConcentration_NACL_value = get_result_by_path(water_analysis_liquid1_trueConcentration_values,['NACL'])
            print(f'NACL value: {water_analysis_liquid1_trueConcentration_NACL_value}')

            #Siderite (FEIICO3) . Unit: mg
            water_analysis_liquid1_trueConcentration_FEIICO3_value = get_result_by_path(water_analysis_liquid1_trueConcentration_values,['FEIICO3'])
            print(f'FEIICO3 value: {water_analysis_liquid1_trueConcentration_FEIICO3_value}')

            #Mackinawite (MACKINAWITE) . Unit: mg
            water_analysis_liquid1_trueConcentration_MACKINAWITE_value = get_result_by_path(water_analysis_liquid1_trueConcentration_values,['MACKINAWITE'])
            print(f'MACKINAWITE value: {water_analysis_liquid1_trueConcentration_MACKINAWITE_value}')

            #Galena (PBS) . Unit: mg
            water_analysis_liquid1_trueConcentration_PBS_value = get_result_by_path(water_analysis_liquid1_trueConcentration_values,['PBS'])
            print(f'PBS value: {water_analysis_liquid1_trueConcentration_PBS_value}')

            #Strontianite (SRCO3) . Unit: mg
            water_analysis_liquid1_trueConcentration_SRCO3_value = get_result_by_path(water_analysis_liquid1_trueConcentration_values,['SRCO3'])
            print(f'SRCO3 value: {water_analysis_liquid1_trueConcentration_SRCO3_value}')

            # example : normalize json output
            # waterAnalysisOutputRecord = get_normalized_waterAnalysis(result_water_analysis['data']['result']['waterAnalysisOutput'])
            # waterAnalysisOutputRecord.insert(loc = 0,column = 'analysis_no', value = row[1])
            # print(f"df: {waterAnalysisOutputRecord}")

            '''
            RECONSTITUE THE INFLOWS FROM THE LIQUID-1 PHASE 
            After the water analysis calculation is run, use the liquid-1 phase molecular outputs to create inflows for the
            TP flash calculation
            '''

            inflows_for_tp_survey_path = "data/result/phases/liquid1/molecularConcentration"
            inflows_for_tp_survey = get_result_by_path(result_water_analysis,inflows_for_tp_survey_path.split('/'))

            #endregion 
            #endregion

            print("#######################################################")
            # print(inflows_for_tp_survey)
            if not inflows_for_tp_survey:
                print("TP calculation can't be completed because molecular concentration data is not available in water analysis output.")
                continue

            '''
            CREATE LIST OF (TEMPERATURE,PRESSURE) FOR TP flash calcultion
            Create a simple Python array containing the list of temperature,pressure to be used in the isothermal
            flash calculations. This was obtained from OLI Studio file -> NEW CES Scenario.
            T -> psig
            P -> F
            '''         
            temperature = get_sensitivity_range(40,240,20)   
            pressure = get_sensitivity_range(100,100,1)

            # upload chemistry file for TP flash calculation
            #result = oliapi.upload_dbs_file("catalyst_tp_flash_01.20.2022.dbs")
            #tp_flash_chemistry_file_id= result["file"][0]["id"]
            tp_flash_chemistry_file_id = "78f92523-bff8-443d-b594-eaaf8ec0457a" 

            # for each combination of temperature and pressure, run isothermal flash calculation
            for pres,temp, in [(pres,temp) for pres in pressure for temp in temperature]:
                print(f'i : {i} , temperature: {temp}, pressure: {pres} ')
                # prepare input for flash calculation using T, P and inflows from above
                isothermal_input = generate_iso_json(pres,temp, inflows_for_tp_survey)

                #region Step 2 : TP flash calculation 
                # call isothermal calculation
                result_flash_calc = oliapi.call("isothermal", tp_flash_chemistry_file_id, isothermal_input)

                tp_out_filename = "%s.json" % ("TP Survey AnalysisNo " + str(row.analysis_no) + " Temperature "+ str(temp))
  
                with open("Data/Input_" + tp_out_filename, 'w', encoding='utf-8') as f:
                    json.dump(isothermal_input, f, ensure_ascii=False, indent=4)
                with open("Data/Output_" + tp_out_filename, 'w', encoding='utf-8') as f:
                    json.dump(result_flash_calc, f, ensure_ascii=False, indent=4)
                
                try:
                    if "status" in result_flash_calc: 
                        status = result_flash_calc["status"]
                        if status == "FAILED":
                            print("TP Calc failed. Skip TP calculation. ") 
                            continue 
                    else: 
                        # result dataset doesn't include status. Skip TP calcualtion. 
                        continue 
                except KeyError:
                    continue   
                
                #region TP flash Scaling Index
                tp_flash_calc_prescalingTendenciesRecord_path = "data/result/additionalProperties/prescalingTendencies/values"
                tp_flash_calc_prescalingTendenciesRecord_values = get_result_by_path(result_flash_calc,tp_flash_calc_prescalingTendenciesRecord_path.split('/'))

                print("**** TP Flash : Scaling index ****")

                #TP calc scaling index data (Unit: None)
                tp_flash_calc_scaling_index_values = convert_prescale_to_scalingindex(tp_flash_calc_prescalingTendenciesRecord_values)

                #Barium Sulfate Barite SL (BASO4). SI Unit: NA
                tp_flash_calc_scaling_index_BASO4_value = get_result_by_path(tp_flash_calc_scaling_index_values,['BASO4'])
                print(f'BASO4 value: {tp_flash_calc_scaling_index_BASO4_value}')

                #Calcite (CACO3) . SI Unit: NA
                tp_flash_calc_scaling_index_CACO3_value = get_result_by_path(tp_flash_calc_scaling_index_values,['CACO3'])
                print(f'CACO3 value: {tp_flash_calc_scaling_index_CACO3_value}')

                #Anhydrite (CASO4) . SI Unit: NA
                tp_flash_calc_scaling_index_CASO4_value = get_result_by_path(tp_flash_calc_scaling_index_values,['CASO4'])
                print(f'CASO4 value: {tp_flash_calc_scaling_index_CASO4_value}')

                #Gypsum (CASO4.2H2O) . SI Unit: NA
                tp_flash_calc_scaling_index_CASO42H2O_value = get_result_by_path(tp_flash_calc_scaling_index_values,['CASO4.2H2O'])
                print(f'CASO4.2H2O value: {tp_flash_calc_scaling_index_CASO42H2O_value}')

                #Celestite (SRSO4) . SI Unit: NA
                tp_flash_calc_scaling_index_SRSO4_value = get_result_by_path(tp_flash_calc_scaling_index_values,['SRSO4'])
                print(f'SRSO4 value: {tp_flash_calc_scaling_index_SRSO4_value}')

                #Sphalerite (ZNS) . SI Unit: NA
                tp_flash_calc_scaling_index_ZNS_value = get_result_by_path(tp_flash_calc_scaling_index_values,['ZNS'])
                print(f'Sphalerite value: {tp_flash_calc_scaling_index_ZNS_value}')

                #Witherite (BACO3) . SI Unit: NA
                tp_flash_calc_scaling_index_BACO3_value = get_result_by_path(tp_flash_calc_scaling_index_values,['BACO3'])
                print(f'BACO3 value: {tp_flash_calc_scaling_index_BACO3_value}')

                #Halite (NACL) . SI Unit: NA
                tp_flash_calc_scaling_index_NACL_value = get_result_by_path(tp_flash_calc_scaling_index_values,['NACL'])
                print(f'NACL value: {tp_flash_calc_scaling_index_NACL_value}')

                #Siderite (FEIICO3) . SI Unit: NA
                tp_flash_calc_scaling_index_FEIICO3_value = get_result_by_path(tp_flash_calc_scaling_index_values,['FEIICO3'])
                print(f'FEIICO3 value: {tp_flash_calc_scaling_index_FEIICO3_value}')

                #Mackinawite (MACKINAWITE) . SI Unit: NA
                tp_flash_calc_scaling_index_MACKINAWITE_value = get_result_by_path(tp_flash_calc_scaling_index_values,['MACKINAWITE'])
                print(f'MACKINAWITE value: {tp_flash_calc_scaling_index_MACKINAWITE_value}')

                #Galena (PBS) . SI Unit: NA
                tp_flash_calc_scaling_index_PBS_value = get_result_by_path(tp_flash_calc_scaling_index_values,['PBS'])
                print(f'PBS value: {tp_flash_calc_scaling_index_PBS_value}')

                #Strontianite (SRCO3) . SI Unit: NA
                tp_flash_calc_scaling_index_SRCO3_value = get_result_by_path(tp_flash_calc_scaling_index_values,['SRCO3'])
                print(f'SRCO3 value: {tp_flash_calc_scaling_index_SRCO3_value}')

                #endregion

                print("**** TP Flash : liquid1 properties ****")

                #region TP flash properties
                tp_flash_calc_liquid1_properties_path = "data/result/phases/liquid1/properties"
                tp_flash_calc_liquid1_properties = get_result_by_path(result_flash_calc,tp_flash_calc_liquid1_properties_path.split('/'))
                #print(tp_flash_calc_liquid1_properties)

                tp_flash_calc_liquid1_hardness = validate_unit_and_fetch_result(tp_flash_calc_liquid1_properties,"hardness","mg/L as CaCO3")
                print(f'hardness value: {tp_flash_calc_liquid1_hardness}')
                tp_flash_calc_liquid1_absoluteViscosity = validate_unit_and_fetch_result(tp_flash_calc_liquid1_properties,"absoluteViscosity","cP")
                print(f'absoluteViscosity value: {tp_flash_calc_liquid1_absoluteViscosity}')
                tp_flash_calc_liquid1_pH = validate_unit_and_fetch_result(tp_flash_calc_liquid1_properties,"ph")
                print(f'pH value: {tp_flash_calc_liquid1_pH}')
                tp_flash_calc_liquid1_alkalinity = validate_unit_and_fetch_result(tp_flash_calc_liquid1_properties,"alkalinity","mg HCO3/L")
                print(f'alkalinity value: {tp_flash_calc_liquid1_alkalinity}')
                tp_flash_calc_liquid1_density = validate_unit_and_fetch_result(tp_flash_calc_liquid1_properties,"density","g/m3")
                #Convert denisityfrom g/m3 to g/cm3. 
                if tp_flash_calc_liquid1_density:
                    tp_flash_calc_liquid1_density = tp_flash_calc_liquid1_density * 0.000001
                else:
                        tp_flash_calc_liquid1_density = 0

                print(f'density value: {tp_flash_calc_liquid1_density}')

                tp_flash_calc_liquid1_specificElectricalConductivity = validate_unit_and_fetch_result(tp_flash_calc_liquid1_properties,"specificElectricalConductivity","mho/cm")
                print(f'specificElectricalConductivity value: {tp_flash_calc_liquid1_specificElectricalConductivity}')
                tp_flash_calc_liquid1_totalDissolvedSolids = validate_unit_and_fetch_result(tp_flash_calc_liquid1_properties,"totalDissolvedSolids","mg/L")
                print(f'totalDissolvedSolids value: {tp_flash_calc_liquid1_totalDissolvedSolids}')

                #endregion 
                print("**** TP Flash : liquid1 concentration ****")
                
                #region liquid1 concentration 
                #Water Analysis liquid1 trueConcentration data (Unit: mg)
                tp_flash_calc_liquid1_trueConcentration_path = "data/result/phases/liquid1/trueConcentration"
                tp_flash_calc_liquid1_trueConcentration = get_result_by_path(result_flash_calc,tp_flash_calc_liquid1_trueConcentration_path.split('/'))
                tp_flash_calc_liquid1_trueConcentration_values = validate_unit_and_fetch_result(tp_flash_calc_liquid1_trueConcentration,"","mg/L")
                #print(tp_flash_calc_liquid1_trueConcentration_values)

                #Aqueous CO2 (CO2). Unit: mg
                tp_flash_calc_liquid1_trueConcentration_CO2_value = get_result_by_path(tp_flash_calc_liquid1_trueConcentration_values,['CO2'])
                print(f'CO2 value: {tp_flash_calc_liquid1_trueConcentration_CO2_value}')

                #Aqueous H2S (H2S). Unit: mg
                tp_flash_calc_liquid1_trueConcentration_H2S_value = get_result_by_path(tp_flash_calc_liquid1_trueConcentration_values,['H2S'])
                print(f'H2S value: {tp_flash_calc_liquid1_trueConcentration_H2S_value}')

                #Barium Sulfate Barite SL (BASO4). Unit: mg
                tp_flash_calc_liquid1_trueConcentration_BASO4_value = get_result_by_path(tp_flash_calc_liquid1_trueConcentration_values,['BASO4'])
                print(f'BASO4 value: {tp_flash_calc_liquid1_trueConcentration_BASO4_value}')

                #Calcite (CACO3) . Unit: mg
                tp_flash_calc_liquid1_trueConcentration_CACO3_value = get_result_by_path(tp_flash_calc_liquid1_trueConcentration_values,['CACO3'])
                print(f'CACO3 value: {tp_flash_calc_liquid1_trueConcentration_CACO3_value}')

                #Anhydrite (CASO4) . Unit: mg
                tp_flash_calc_liquid1_trueConcentration_CASO4_value = get_result_by_path(tp_flash_calc_liquid1_trueConcentration_values,['CASO4'])
                print(f'CASO4 value: {tp_flash_calc_liquid1_trueConcentration_CASO4_value}')

                #Gypsum (CASO4.2H2O) . Unit: mg
                tp_flash_calc_liquid1_trueConcentration_CASO42H2O_value = get_result_by_path(tp_flash_calc_liquid1_trueConcentration_values,['CASO4.2H2O'])
                print(f'CASO4.2H2O value: {tp_flash_calc_liquid1_trueConcentration_CASO42H2O_value}')

                #Celestite (SRSO4) . Unit: mg
                tp_flash_calc_liquid1_trueConcentration_SRSO4_value = get_result_by_path(tp_flash_calc_liquid1_trueConcentration_values,['SRSO4'])
                print(f'SRSO4 value: {tp_flash_calc_liquid1_trueConcentration_SRSO4_value}')

                #Sphalerite (ZNS) . Unit: mg
                tp_flash_calc_liquid1_trueConcentration_ZNS_value = get_result_by_path(tp_flash_calc_liquid1_trueConcentration_values,['ZNS'])
                print(f'Sphalerite value: {tp_flash_calc_liquid1_trueConcentration_ZNS_value}')

                #Witherite (BACO3) . Unit: mg
                tp_flash_calc_liquid1_trueConcentration_BACO3_value = get_result_by_path(tp_flash_calc_liquid1_trueConcentration_values,['BACO3'])
                print(f'BACO3 value: {tp_flash_calc_liquid1_trueConcentration_BACO3_value}')

                #Halite (NACL) . Unit: mg
                tp_flash_calc_liquid1_trueConcentration_NACL_value = get_result_by_path(tp_flash_calc_liquid1_trueConcentration_values,['NACL'])
                print(f'NACL value: {tp_flash_calc_liquid1_trueConcentration_NACL_value}')

                #Siderite (FEIICO3) . Unit: mg
                tp_flash_calc_liquid1_trueConcentration_FEIICO3_value = get_result_by_path(tp_flash_calc_liquid1_trueConcentration_values,['FEIICO3'])
                print(f'FEIICO3 value: {tp_flash_calc_liquid1_trueConcentration_FEIICO3_value}')

                #Mackinawite (MACKINAWITE) . Unit: mg
                tp_flash_calc_liquid1_trueConcentration_MACKINAWITE_value = get_result_by_path(tp_flash_calc_liquid1_trueConcentration_values,['MACKINAWITE'])
                print(f'MACKINAWITE value: {tp_flash_calc_liquid1_trueConcentration_MACKINAWITE_value}')

                #Galena (PBS) . Unit: mg
                tp_flash_calc_liquid1_trueConcentration_PBS_value = get_result_by_path(tp_flash_calc_liquid1_trueConcentration_values,['PBS'])
                print(f'PBS value: {tp_flash_calc_liquid1_trueConcentration_PBS_value}')

                #Strontianite (SRCO3) . Unit: mg
                tp_flash_calc_liquid1_trueConcentration_SRCO3_value = get_result_by_path(tp_flash_calc_liquid1_trueConcentration_values,['SRCO3'])
                print(f'SRCO3 value: {tp_flash_calc_liquid1_trueConcentration_SRCO3_value}')

                #endregion 

                print("**** TP Flash : solid concentration ****")

                #region TP flash solid1
                #Water Analysis solid1 trueConcentration data (Unit: mg)
                tp_flash_calc_solid_molecularConcentration_path = "data/result/phases/solid/molecularConcentration"
                tp_flash_calc_solid_molecularConcentration = get_result_by_path(result_flash_calc,tp_flash_calc_solid_molecularConcentration_path.split('/'))
                tp_flash_calc_solid_molecularConcentration_values = validate_unit_and_fetch_result(tp_flash_calc_solid_molecularConcentration,"","mg/L")

                # convert mg/L to lb/1000 BBL
                tp_flash_calc_solid_trueConcentration_perBBL_values = convert_mgl_to_per1000BBL(tp_flash_calc_solid_molecularConcentration_values)

                #Barium Sulfate Barite SL (BASO4). Unit: lb/BBL
                tp_flash_calc_solid_trueConcentration_BASO4_value = get_result_by_path(tp_flash_calc_solid_trueConcentration_perBBL_values,['BASO4'])
                print(f'BASO4 value: {tp_flash_calc_solid_trueConcentration_BASO4_value}')

                #Calcite (CACO3) . Unit: lb/BBL
                tp_flash_calc_solid_trueConcentration_CACO3_value = get_result_by_path(tp_flash_calc_solid_trueConcentration_perBBL_values,['CACO3'])
                print(f'CACO3 value: {tp_flash_calc_solid_trueConcentration_CACO3_value}')

                #Anhydrite (CASO4) . Unit: lb/BBL
                tp_flash_calc_solid_trueConcentration_CASO4_value = get_result_by_path(tp_flash_calc_solid_trueConcentration_perBBL_values,['CASO4'])
                print(f'CASO4 value: {tp_flash_calc_solid_trueConcentration_CASO4_value}')

                #Gypsum (CASO4.2H2O) . Unit: lb/BBL
                tp_flash_calc_solid_trueConcentration_CASO42H2O_value = get_result_by_path(tp_flash_calc_solid_trueConcentration_perBBL_values,['CASO4.2H2O'])
                print(f'CASO4.2H2O value: {tp_flash_calc_solid_trueConcentration_CASO42H2O_value}')

                #Celestite (SRSO4) . Unit: lb/BBL
                tp_flash_calc_solid_trueConcentration_SRSO4_value = get_result_by_path(tp_flash_calc_solid_trueConcentration_perBBL_values,['SRSO4'])
                print(f'SRSO4 value: {tp_flash_calc_solid_trueConcentration_SRSO4_value}')

                #Sphalerite (ZNS) . Unit: lb/BBL
                tp_flash_calc_solid_trueConcentration_ZNS_value = get_result_by_path(tp_flash_calc_solid_trueConcentration_perBBL_values,['ZNS'])
                print(f'Sphalerite value: {tp_flash_calc_solid_trueConcentration_ZNS_value}')

                #Witherite (BACO3) . Unit: lb/BBL
                tp_flash_calc_solid_trueConcentration_BACO3_value = get_result_by_path(tp_flash_calc_solid_trueConcentration_perBBL_values,['BACO3'])
                print(f'BACO3 value: {tp_flash_calc_solid_trueConcentration_BACO3_value}')

                #Halite (NACL) . Unit: lb/BBL
                tp_flash_calc_solid_trueConcentration_NACL_value = get_result_by_path(tp_flash_calc_solid_trueConcentration_perBBL_values,['NACL'])
                print(f'NACL value: {tp_flash_calc_solid_trueConcentration_NACL_value}')

                #Siderite (FEIICO3) . Unit: lb/BBL
                tp_flash_calc_solid_trueConcentration_FEIICO3_value = get_result_by_path(tp_flash_calc_solid_trueConcentration_perBBL_values,['FEIICO3'])
                print(f'FEIICO3 value: {tp_flash_calc_solid_trueConcentration_FEIICO3_value}')

                #Mackinawite (MACKINAWITE) . Unit: lb/BBL
                tp_flash_calc_solid_trueConcentration_MACKINAWITE_value = get_result_by_path(tp_flash_calc_solid_trueConcentration_perBBL_values,['MACKINAWITE'])
                print(f'MACKINAWITE value: {tp_flash_calc_solid_trueConcentration_MACKINAWITE_value}')

                #Galena (PBS) . Unit: lb/BBL
                tp_flash_calc_solid_trueConcentration_PBS_value = get_result_by_path(tp_flash_calc_solid_trueConcentration_perBBL_values,['PBS'])
                print(f'PBS value: {tp_flash_calc_solid_trueConcentration_PBS_value}')

                #Strontianite (SRCO3) . Unit: lb/BBL
                tp_flash_calc_solid_trueConcentration_SRCO3_value = get_result_by_path(tp_flash_calc_solid_trueConcentration_perBBL_values,['SRCO3'])
                print(f'SRCO3 value: {tp_flash_calc_solid_trueConcentration_SRCO3_value}')

                #endregion
                #endregion
                print("*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*")

            i = i+1

        con.close

